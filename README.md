# DevOps lessons

This are my notes for learning DevOps, feel free to use them as needed.

## Fases del ciclo de DevOps

1. Planear
2. Código
3. Construir artefactos
4. Probar
5. Release
6. Deployment
7. Operar
8. Monitoreo

Y vuelta otra vez.

Corregir errores, generar nueva arquitectura, o cambiarla, y ver como operar de
manera mas eficiente. 

## DevOps Engineering

Uso práctico de DevOps, ser capaz de construir, probar, release y monitorear
aplicaciones.

## 3 pillars of DevOps Engineering

1. Pull request automation
2. Deployment automation
3. Manejo de funcionamiento de la app (Application performance management)

### Pull request automation

- Developer share code changes in git or version contorl
- A set of code changes is a pull request or merge request
- If pull requests are approved, they are automatically deployed

#### Git

- Version Control system, code review, linting, test automation
- Feedback from product managers, engineering managers, marketers, C-Suite
- Continuous integration
- Per change ephemeral environments
- Automated security scanning
- Notifications to reviewers
- Help developers change proposals get reviewed and merged within 24 hours 

### Deployment automation

- Deploy a feature to a certain set of users, as a final test before public
  rollout
- Starting new versions of services without causing downtime
- Rolling back to a previous version in case of failure
- Use off-the-shelf components for deployments
- Try to minimize custom code
- Simplify deployment processes

### Application performance Management

- Metrics: numeric measures of key numbers in production
- Logging: text descriptions of what is happening during operation
- Monitoring: Take metrics and logs and convert to health metrics
- Alerting: If monitoring detects a problem, notify developers

## Examples of DevOps engineering

New startup with no users building a web app

Recommended stack (Free tiers):
- github/gitlab
- Netlify/Vercel
- LayerCI

For small number of enterprise users:
- github/gitlab
- Sentry
- PagerDuty
- CodeCov
- Bitrise/CircleCI

Social Media App (Reddit)
- Github enterprise
- Sentry
- ELK stack
- Pingdom
- Launch Darkly
- Terraform

**DEvOps Engineering is vital for engineering teams, especially as the product
matures**

## Test driven development

- What is test driven development
- unit tests and integration tests come from factories and manufacvturing
- integration tests 
System and end-to-end tests

### TDD Goals

To write tests for all features requested and write code that passes those tests.

### Before or without TDD

- choose something to work on
- Build it based on specifications
- Test it with small scripts

### TDD methodology

- choose something to work on
- write tests that would pass if product works
- keep building the product until all tests pass

## What is Continuous Integration (CI)

Pushing many small changes to a central repository, these changes are
automatically verified by a test suite, to ensure no major issues are seen by 
customers.

## Top 3 benefits of CI

- is the first step to DevOps automation and helps with code collaboration. 
- CI improves developer speed without breaking existing code
- CI helps reduce customer churn and user satisfaction by avoiding broken code


FEATURE-BRANCH workflow here...
Gitflow workflow

List several workflows

## CI/CD

- github actions, gitlab workflows
- CI is a vital tool for developer collabortation, Increase collaboration, 
prevent errors, and increase user satisfaction.

Tutorial time

- pull the change locally, build and review (no CI/CD)

with CI/CD (in LayerCI)

- cypress tests 
- layerci install into github repository
- setup a configuration file and run cypress
- docker compose
- install docker with a docker file
- docker compose
- copy repository files
- build services
- install services
- deploy pipeline
- run tests
- add configuration to repository
- and run
- run linters, other checks
- github actions

## What is code coverage?

- Measures how comprehensive a code base's tests are.
- unit tests
- enforce that tests are written in an objective way


syntax lines (don't make sense to test)
logic lines (make sense to test)
branch lines (for loops, if statements)

code coverage formula:

```
non-syntax lines with tests / total number of non-syntax lines
```

branch coverage formula

```
number of branches covered by a test / total number of branches
```

when  to care?

- if product has users, and users might leave if they are affected by bugs
- you are working with developers that aren't immediatly trustworthy like contractors/interns
- you are working with a large codebase

**Common mistake:** too many tests for uncertain features

- code coverage must not decrease
- code owners for test files

## CI Continuous integration

- Linting: programs that review source code (static review)
and help avoid obvious bugs
- code style (conventions, tabs or spaces, camelCase, pothole_case)
- PEP-8 style for python
- Nit Approach definition: nits are little comments on the code that the team 
can ignore until broader changes are made
- auto formatters (gofmt for golang) 

**Easy solution in CI: no merging if linting fails**

**Better solution in CI: Automatically fixes code issues amd merge** creates a 
linted branch, and if there are no errors in the unit tests, can be merged

python: pylint, flake8, SonarQube, DeepSource

- Any team with more than one developer working in the same codebase should 
setup a linter to catch obvious bugs.

## Ephemeral environments

- Temporal environments that have a self-contained version of the application, 
generally every feature branch
- Temporal environments are made on every change and can review a change for 
everyone not just devs
- Are half-way between development and staging
- Accelerates software develo0pment lifecycle
Allos changes to be shared with managers and other stakeholders
- Problem is associated with the state, like users, and databases, like a small 
copy of the database

### How do databases work in ephemeral environments

- Have prepopulated data: to pass security audits use synthetic or anonimized 
data
- Undoable: if data is deleted or modified, should be easy to reset
- Migrated: Uses schema currently in production, and has proposed migrations run
against it, helps to discover broken or non-performant database migrations

### Lifecycle of ephemeral environments

- Option A: tie the lifecycle to the life of a pull request or merge request
- Option B: Create a new environment with a small timeout
- Option C: Create a new environment for every commit and hibernate them the 
second they are provisioned, wake them up as required 

## Continuous staging

- CI/CD is merged with ephemeral environments to form an unified CI/CD and 
review process for every commit

6-12 months of engineering time for setup, then dedicated person for managing 
these environments

will need a cloud service provider

create a bot that responds to a request creating an environment for a specific 
branch or pull request

## Containers

### How containers work

running a program in a VM or a container
The big change is that shared resources are shared through files
namespaces: linux trick to share resources
containers are kind of sandboxes that share resources and have different 
environment variables and files from the original system ones

### How VMs work

the idea of VMs is to provide fake versions of CPU, RAM, disk, and devices

hypervisors fake the resources from a VM

Containers info is read to and setup to files, instead of devices

CPUs are slower in VMs than in containers
VMs use more storage

VMs are better running untrusted code
If you are running another operating system
If you need to emulate hardware


## Deployment strategies

### Rolling deployments

Strategy to roll a new app without causing downtime

- create a new instance
- wait until it is up
- delete an instance of the old version
- repeat until all instances are replaced
- (needs many servers)

Benefits

- well supported
- no huge bursts of costs
- easily reverted

### Blue/green deployments

- strategy to deploy a new version of the app
- connect to shared resource (DB)
- New version is green, when testing is done green is replaced by blue, and the
cycle continues


Benefits: 

- Easy to understand
- powerful
- extendable to workflows

Downsides 

- Difficult to deploy a hotfix
- resource allocation is not convenient
- clusters can affect each others

### rainbow deployments

- for long running tasks
- teams have many clusters ready
- old clusters are only shut off after all the long running jobs have finished
- Acceptance tests


## Authors and acknowledgment

Otto Hahn Herrera

## License

Use as needed, no need to attribute.

## Project status

Ongoing

## References

- https://www.youtube.com/watch?v=lpk7VpGqkKw
- https://www.youtube.com/watch?v=j5Zsa_eOXeY
